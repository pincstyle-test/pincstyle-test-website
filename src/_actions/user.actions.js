/**
 * Created by f.putra on 2019-05-19.
 */
import { userConstant } from "../_constants";
import { userServices } from "../_networks";
import { history } from "../_helpers";

export const userActions = {
  save,
  update,
  read,
  login,
  delete: _delete
};
function save(payload) {
  return dispatch => {
    dispatch(request());
    userServices
      .save(payload)
      .then(
        result => dispatch(success(result)),
        error => dispatch(failure(error))
      );
  };

  function request() {
    return { type: userConstant.USER_SAVE_REQUEST };
  }

  function success(result) {
    return { type: userConstant.USER_SAVE_SUCCESS, result };
  }

  function failure(error) {
    return { type: userConstant.USER_SAVE_FAILURE, error };
  }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    userServices
      .delete(id)
      .then(id => dispatch(success(id)), error => dispatch(failure(id, error)));
  };

  function request(id) {
    return { type: userConstant.USER_DELETE_REQUEST, id };
  }

  function success(id) {
    return { type: userConstant.USER_DELETE_SUCCESS, id };
  }

  function failure(id, error) {
    return { type: userConstant.USER_DELETE_FAILURE, id, error };
  }
}

function read(offset, limit) {
  return dispatch => {
    dispatch(request(offset, limit));
    userServices
      .read(offset, limit)
      .then(
        users => dispatch(success(users)),
        error => dispatch(failure(error))
      );
  };

  function request(offset, limit) {
    return { type: userConstant.USER_READ_REQUEST, offset, limit };
  }

  function success(users) {
    return { type: userConstant.USER_READ_SUCCESS, users };
  }

  function failure(error) {
    return { type: userConstant.USER_READ_FAILURE, error };
  }
}

function update(payload) {
  return dispatch => {
    dispatch(request());
    userServices
      .update(payload)
      .then(
        result => dispatch(success(result)),
        error => dispatch(failure(error))
      );
  };

  function request() {
    return { type: userConstant.USER_UPDATE_REQUEST };
  }

  function success(result) {
    return { type: userConstant.USER_UPDATE_SUCCESS, result };
  }

  function failure(error) {
    return { type: userConstant.USER_UPDATE_FAILURE, error };
  }
}

function login(username, password) {
  return dispatch => {
    dispatch(request({ username }));
    userServices.login(username, password).then(
      user => {
        dispatch(success(user));
        if (user.status === 200) {
          localStorage.setItem("user", JSON.stringify(user.message));
          localStorage.setItem("tc", user.message.token);
          history.push("/app/landing");
          window.location.reload();
        }
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request(user) {
    return { type: userConstant.LOGIN_REQUEST, user };
  }

  function success(user) {
    return { type: userConstant.LOGIN_SUCCESS, user };
  }

  function failure(error) {
    return { type: userConstant.LOGIN_FAILURE, error };
  }
}
