import LandingPage from "views/LandingPage/LandingPage.jsx";
import Login from "views/Credentials/Login.jsx";

// @material-ui/icons
import DashboardIcon from "@material-ui/icons/Dashboard";

var dashRoutes = [
  {
    path: "/landing",
    name: "Landing",
    icon: DashboardIcon,
    component: LandingPage,
    layout: "/app"
  },
  {
    path: "/login-page",
    name: "Login",
    component: Login,
    layout: "/auth"
  }
];
export default dashRoutes;
