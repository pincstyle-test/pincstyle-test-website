/**
 * Created by f.putra on 2019-07-01.
 */
import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isLogin } from "_helpers";

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page

    <Route
      {...rest}
      render={props =>
        isLogin() ? (
          <Component {...props} />
        ) : (
          <Redirect to="/auth/login-page" />
        )
      }
    />
  );
};

export default PrivateRoute;
