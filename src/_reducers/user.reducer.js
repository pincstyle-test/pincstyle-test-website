/**
 * Created by f.putra on 2019-05-19.
 */
import { userConstant } from "_constants";

export function users(state = { offset: 0, page: 0 }, action) {
  switch (action.type) {
    case userConstant.USER_SAVE_REQUEST:
      return {
        ...state
      };
    case userConstant.USER_SAVE_SUCCESS:
      state.items.rows.push(
          {
            id: action.result.message.id,
            name: action.result.message.name,
            email: action.result.message.email,
          }
      )
      state.items.count = state.items.count + 1
      return {
        ...state
      };
    case userConstant.USER_SAVE_FAILURE:
      return {
        ...state
      };
    case userConstant.USER_UPDATE_REQUEST:
      return {
        ...state
      };
    case userConstant.USER_UPDATE_SUCCESS:
      let index
      state.items.rows.map((value, key) => {
        if (action.result.message.id === value.id) {
          index = key
        }
      })
      state.items.rows[index].name = action.result.message.name;
      state.items.rows[index].email = action.result.message.email;

      return {
        ...state
      };
    case userConstant.USER_UPDATE_FAILURE:
      return {
        ...state
      };
    case userConstant.USER_READ_REQUEST:
      return {
        ...state,
        pageInit : action.offset / action.limit,
      };
    case userConstant.USER_READ_SUCCESS:
      return {
        ...state,
        page : state.pageInit,
        items: action.users.message
      };
    case userConstant.USER_READ_FAILURE:
      return {
        ...state,
        error: action.error
      };
    case userConstant.USER_DELETE_REQUEST:
      return {
        ...state,
        deleteId: action.id
      };
    case userConstant.USER_DELETE_SUCCESS:
      return {
        ...state,
        items: {
          count: state.items.count = state.items.count - 1,
          rows: state.items.rows.filter(user => user.id !== state.deleteId)
        }
      };
    case userConstant.USER_DELETE_FAILURE:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
}
