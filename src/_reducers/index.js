/**
 * Created by f.putra on 13/06/18.
 */
import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

import { users } from "./user.reducer";

const rootReducer = combineReducers({
  users,
  routerReducer
});

export default rootReducer;
