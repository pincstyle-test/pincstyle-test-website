import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Route, Router, Switch } from "react-router-dom";
import { store, history } from "./_helpers";
import { CookiesProvider } from "react-cookie";
import * as serviceWorker from "./serviceWorker";
import AuthLayout from "layouts/Auth.jsx";
import AppLayout from "views/LandingPage/LandingPage.jsx";
import PrivateRoute from "layouts/PrivateRoute.jsx";

import "assets/scss/test.scss?v=0.1.0";

ReactDOM.render(
  <CookiesProvider>
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path={"/"} component={AuthLayout} />
          <Route exact path={"/auth/**"} component={AuthLayout} />
          <PrivateRoute exact path={"/app/**"} component={AppLayout} />
        </Switch>
      </Router>
    </Provider>
  </CookiesProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
