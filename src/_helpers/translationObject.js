/**
 * Created by f.putra on 2019-04-09.
 */
export const translationsObject = {
    en: {
        application: {
            title: 'Awesome app with i18n!',
            hello: 'Hello, %{name}!'
        },
        date: {
            long: 'MMMM Do, YYYY'
        },
        export: 'Export %{count} items',
        export_0: 'Nothing to export',
        export_1: 'Export %{count} item',
        two_lines: 'Line 1<br />Line 2',
        literal_two_lines: 'Line 1'
    },
    nl: {
        application: {
            title: 'Toffe app met i18n!',
            hello: 'Hallo, %{name}!'
        },
        date: {
            long: 'D MMMM YYYY'
        },
        export: 'Exporteer %{count} dingen',
        export_0: 'Niks te exporteren',
        export_1: 'Exporteer %{count} ding',
        two_lines: 'Regel 1<br />Regel 2',
        literal_two_lines: 'Regel 1 Regel 2'
    }
};
