/**
 * Created by f.putra on 13/06/18.
 */
import {applyMiddleware, createStore, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import literals from "./literals.js";
import state from '../_reducers';

const middlewares = [];

if (process.env.NODE_ENV === `development`) {
    const { logger } = require(`redux-logger`);
    middlewares.push(logger);
}

export const store = createStore(
    combineReducers({
        state,
        literals
    }),
    applyMiddleware(
        thunkMiddleware,
        ...middlewares
    )
);
