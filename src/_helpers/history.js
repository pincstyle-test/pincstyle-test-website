/**
 * Created by f.putra on 13/06/18.
 */
import {createHashHistory} from 'history';

export const history = createHashHistory();