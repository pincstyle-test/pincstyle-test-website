/**
 * Created by f.putra on 13/06/18.
 */
export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));
    let tc = localStorage.getItem('tc');

    if (user && tc) {
        return { 'Authorization': 'Bearer ' + tc };
    } else {
        return {};
    }
}
