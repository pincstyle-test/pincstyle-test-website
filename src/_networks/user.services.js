/**
 * Created by f.putra on 2019-05-19.
 *
 * @UPDATE
 * READ
 * DELETE
 */
import request from "./request";
import { authHeader } from "_helpers";

export const userServices = {
  save,
  update,
  read,
  login,
  delete: _delete
};

let url = "/users";

function login(username, password) {
  return request({
    url: url + "/login",
    method: "POST",
    auth: {
      username: username,
      password: password
    }
  });
}

function save(payload) {
  return request({
    url: url + "/register",
    method: "POST",
    headers : {
      "Content-Type" : "application/json"
    },
    data: payload
  });
}

function update(payload) {
  return request({
    url: url + "/update",
    method: "PATCH",
    headers : {
      "Content-Type" : "application/json"
    },
    data: payload
  });
}

async function read(offset, limit) {
  return request({
    url: url + "/offset=" + offset + "&limit=" + limit,
    method: "GET"
  });
}

function _delete(payload) {
  return request({
    url: url + "/" + payload,
    method: "DELETE"
  });
}
