/**
 * Created by f.putra on 2019-06-19.
 */
import { cardTitle } from "assets/jss/test.jsx";

const masterUserStyle = theme => ({
  customCardContentClass: {
    paddingLeft: "0",
    paddingRight: "0"
  },
  cardIconTitle: {
    ...cardTitle,
    marginTop: "15px",
    marginBottom: "0px"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  },
  grid: {
    width: "60%"
  },
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
});
export default masterUserStyle;
