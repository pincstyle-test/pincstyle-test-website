/**
 * Created by f.putra on 2019-08-16.
 */
import React from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal-resizable-draggable";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// material-ui icons
import Assignment from "@material-ui/icons/Assignment";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import TablePagination from "@material-ui/core/TablePagination";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import GridList from "@material-ui/core/GridList";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import style from "assets/jss/views/masterUserStyle";
import { connect } from "react-redux";
import { userActions } from "_actions";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

class MasterUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      data: [],
      page: 0,
      rowsPerPage: 5,
      value: 0,
      id: "",
      name: "",
      email: "",
      password: ""
    };
  }

  componentWillMount() {
    const { dispatch, users } = this.props;
    const { rowsPerPage } = this.state;
    dispatch(userActions.read(users.offset, rowsPerPage));
  }

  handleChangePage = (event, page) => {
    const { dispatch } = this.props;
    const { rowsPerPage } = this.state;
    dispatch(userActions.read(page * rowsPerPage, rowsPerPage));
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    const { dispatch, users } = this.props;
    dispatch(userActions.read(users.offset, event.target.value));
    this.setState({ rowsPerPage: event.target.value });
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleInput = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleDateInput(param) {
    this.setState({ date_birth: param });
  }

  async handleActions(name, param) {
    const { dispatch } = this.props;
    switch (name) {
      case "edit":
        this.setState({
          modal: true,
          id: param.id,
          name: param.name,
          email: param.email
        });
        break;
      case "delete":
        dispatch(userActions.delete(param.id));
        break;
    }
  }

  handleButton(name, param) {
    // const {dispatch} = this.props;
    switch (name) {
      case "add":
        this.setState({ modal: true });
        break;
    }
  }

  handleCloseModal = () => {
    this.setState({ modal: false });
  };

  async handleSubmit(menu) {
    const { modal, id, name, email, password } = this.state;
    const { dispatch } = this.props;
    switch (menu) {
      case "add":
        dispatch(
          userActions.save({
            name: name,
            email: email,
            password: password
          })
        );
        this.setState({ modal: false });
        this.clearData();
        this.forceUpdate();

        break;
      case "edit":
        dispatch(
          userActions.update({
            id: id,
            name: name,
            email: email
          })
        );
        this.setState({ modal: false });
        this.clearData();
        this.forceUpdate();
        break;
    }
  }

  clearData() {
    this.setState({
      id: "",
      name: "",
      email: "",
      password: ""
    });
  }

  render() {
    const { classes, users } = this.props;
    const { value, rowsPerPage, modal, id, name, email, password } = this.state;

    return (
      <GridContainer>
        <ReactModal
          initWidth={800}
          initHeight={400}
          onRequestClose={() => this.handleCloseModal()}
          isOpen={modal}
        >
          <Tabs value={value} onChange={this.handleChange}>
            <Tab label="Data User" />
          </Tabs>
          <div className="body">
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                {value === 0 && (
                  <TabContainer>
                    <form
                      className={classes.container}
                      noValidate
                      autoComplete="off"
                    >
                      <TextField
                        label="Name"
                        className={classes.textField}
                        value={name}
                        onChange={this.handleInput("name")}
                        margin="normal"
                      />
                      <TextField
                        label="Email"
                        className={classes.textField}
                        value={email}
                        onChange={this.handleInput("email")}
                        margin="normal"
                      />
                      <TextField
                        label="Password"
                        className={classes.textField}
                        value={password}
                        onChange={this.handleInput("password")}
                        margin="normal"
                      />
                    </form>
                    <div>
                      <Button
                        variant="outlined"
                        color="primary"
                        className={classes.button}
                        onClick={() => this.handleCloseModal()}
                      >
                        CLOSE
                      </Button>
                      <Button
                        variant="outlined"
                        color="primary"
                        className={classes.button}
                        onClick={this.handleSubmit.bind(
                          this,
                          id === "" ? "add" : "edit"
                        )}
                      >
                        SAVE
                      </Button>
                    </div>
                  </TabContainer>
                )}
              </GridItem>
            </GridContainer>
          </div>
        </ReactModal>
        <GridItem xs={12}>
          <Card>
            <CardHeader color="rose" icon>
              <CardIcon color="rose">
                <Assignment />
              </CardIcon>
              <h4 className={classes.cardIconTitle}>Master User</h4>
              <Button
                color={"primary"}
                customClass={classes.actionButton}
                style={{ float: "right" }}
                onClick={() => this.handleButton("add")}
              >
                Add
              </Button>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Name", "Email", "Actions"]}
                tableData={
                  users.items !== undefined
                    ? users.items.rows.map((prop, key) => [
                        prop.name,
                        prop.email,
                        [
                          <Button
                            color={"success"}
                            onClick={this.handleActions.bind(
                              this,
                              "edit",
                              prop
                            )}
                            customClass={classes.actionButton}
                            key={key + 2}
                          >
                            <Edit className={classes.icon} />
                          </Button>,
                          <Button
                            color={"secondary"}
                            onClick={this.handleActions.bind(
                              this,
                              "delete",
                              prop
                            )}
                            customClass={classes.actionButton}
                            key={key + 3}
                          >
                            <Close className={classes.icon} />
                          </Button>
                        ]
                      ])
                    : [[]]
                }
                coloredColls={[4]}
                colorsColls={["primary"]}
                // 0 is for classes.center, 4 is for classes.right, 5 is for classes.right
                customClassesForCells={[0, 3, 4, 5]}
                customHeadCellClasses={[
                  classes.center,
                  classes.center,
                  classes.right,
                  classes.right
                ]}
                // 0 is for classes.center, 4 is for classes.right, 5 is for classes.right
                customHeadClassesForCells={[0, 3, 4, 5]}
              />
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={users.items !== undefined ? users.items.count : 0}
                rowsPerPage={rowsPerPage}
                page={users.page}
                backIconButtonProps={{
                  "aria-label": "Previous Page"
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page"
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

MasterUser.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

function mapStateToProps({ state }) {
  const { users, role } = state;
  return {
    users,
    role
  };
}

export default withStyles(style)(connect(mapStateToProps)(MasterUser));
