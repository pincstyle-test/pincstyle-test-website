# build environment
FROM node:12.8.0-alpine as react-builder
WORKDIR /app
COPY . ./
RUN yarn install
RUN yarn build

# production environment
# Stage 2 - the production environment
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-builder /app/build /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
